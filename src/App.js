import React, {useEffect, useReducer, useState} from 'react';
import sort from 'fast-sort';
import logo from './logo.svg';
import './App.css';

function App() {

  const reducer = (state, action) => {
    switch (action.type) {
      case 'init':
        return {...action.images, totalImages: action.images.length}
      case 'add':
        let newState = {...state}
        newState[action.id]['total'] ? newState[action.id]['total']++ : newState[action.id]['total'] = 1
        return {...newState}
      default:
        throw new Error('Action type not found in the reducer');
    }
  }

  const [images, dispatch] = useReducer(reducer, null)
  const [route, setRoute] = useState('battle')

  useEffect(() => {
    //move in constructor class
    if (images === null) {
      // https://latelier.co/data/cats.json => cross origin problem
      // @todo: move httpresponse file in src
      fetch('httpResponse.json')
        .then(response => {
          return response.json();
        })
        .then(json => {
          dispatch({type: 'init', images: [...json.images]})
        })
    }
  })

  const renderCat = () => {
    if (images) {
      const id = Math.floor(Math.random() * images.totalImages)
      return <img src={images && images[id].url} onClick={() => dispatch({type: 'add', id})}
                  key={`${id}-${Math.random()}`} style={{height: '200px'}}/>
    }
  }

  const orderResults = (images) => {

    let newImages = []

    for (let id in images) {
      if (id !== 'totalImages') {
        let image = images[id]
        if (image.total === undefined) image.total = 0
        newImages.push(image)
      }
    }

    sort(newImages).desc(i => i.total)

    return newImages

  }

  const renderBattle = () => {

    return [renderCat(),
      <img src={logo} className="App-logo" alt="logo" key={'react'}/>, renderCat(), renderLink(route)]
  }

  const renderLink = (route) => {

    const title = (route === 'battle') ? 'top classement' : 'return to battle'
    const path = (route === 'battle') ? 'top' : 'battle'

    return <a className="App-link" target="#" onClick={() => setRoute(path)} key={'link'}> {title} </a>

  }

  const renderTop = () => {

    const sortResults = orderResults(images)
    console.log(sortResults)
    const img = sortResults.map((el) => {
      return (
        <div key={el.id}>
          <img src={el.url} key={`${el.total}-${Math.random()}`} style={{height: '200px'}} alt='no warn'/>
          <p>total: {`${el.total}`}</p>
        </div>
      )
    })

    return [renderLink('top'), ...img]

  }


  return (
    <div className="App">
      <header className="App-header">
        {route === 'battle' && renderBattle()}
        {route === 'top' && renderTop()}
      </header>
    </div>
  );
}

export default App;
